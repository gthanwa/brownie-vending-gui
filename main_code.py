import serial
import time
import ST
from ttkbootstrap import Style
import tkinter as tk
from tkinter import ttk
from PIL import Image,ImageTk
import serial
import time

ser = serial.Serial('COM2',9600)
if not(ser.is_open):
    ser.open()    
time.sleep(1)

def sayhi():
            ST.tts("ยินดีต้อนรับค่ะ เชิญเลือกซื้อสินค้าได้ตามสบายค่ะ",'th')
            time.sleep(1.5)
            ser.write(str.encode("s"))
            print("send s") 

def send_1():
    ser.write(str.encode("1"))
    print("send 1")

def send_2():
    ser.write(str.encode("2"))
    print("send 2")

def send_3():
    ser.write(str.encode("3"))
    print("send 3")

def send_cf():
    ST.tts("ราคาสินค้าทั้งหมดแสดงที่หน้าจอค่ะ",'th')
    ser.write(str.encode("f"))
    print("send f")


def send_cc():
    ST.tts("ขอบคุณที่ใช้บริการค่ะ",'th')
    ser.write(str.encode("c"))
    print("send c")   
    
style = Style(theme="minty")
window = style.master
window.title("Autobotshopping")
window.geometry("1500x800")

lb1 = ttk.Label(window,text="1.เริ่มต้นการใช้งาน",font=("Helvetica 26 bold"))
lb1.place(x=20,y=50,width=350)
hcimage = ImageTk.PhotoImage(Image.open("hc.png").resize((250,370)))
lb2 = ttk.Label(window,image=hcimage)
lb2.place(x=25,y=100)


lb3 = ttk.Label(window,text="2.เลือกสินค้าที่ต้องการ",font=("Helvetica 26 bold"))
lb3.place(x=550,y=50,width=450)

brownimg = ImageTk.PhotoImage(Image.open("brown.png").resize((200,150)))
lb4 = ttk.Label(window,image=brownimg)
lb4.place(x=450,y=230)

lavaimage = ImageTk.PhotoImage(Image.open("lava.png").resize((200,150)))
lb5 = ttk.Label(window,image=lavaimage)
lb5.place(x=750,y=230)

bnlaimage = ImageTk.PhotoImage(Image.open("brownlava.png").resize((200,150)))
lb6 = ttk.Label(window,image=bnlaimage)
lb6.place(x=690,y=460)

cat2image = ImageTk.PhotoImage(Image.open("cat2.png").resize((250,370)))
lb7 = ttk.Label(window,image=cat2image)
lb7.place(x=420,y=400)

lb8 = ttk.Label(window,text="3.ยืนยันการเลือกซื้อสินค้า",font=("Helvetica 26 bold"))
lb8.place(x=1050,y=50,width=550)

cat3image = ImageTk.PhotoImage(Image.open("cat3.png").resize((250,370)))
lb9 = ttk.Label(window,image=cat3image)
lb9.place(x=1100,y=90)


style.configure("Solid.TButton",font=("Helvetica 18"))
style.configure("info.TButton",font=("Helvetica 18"))
style.configure("Outline.TButton",font=("Helvetica 18"))

bt1 = ttk.Button(window,text="ตกลง",cursor="hand2",style="success.Solid.TButton",command=sayhi)
bt1.place(x=50,y=430,width=200)

bt2 = ttk.Button(window,text="กด 1 บราวนี่",cursor="hand2",style="info.TButton",command=send_1)
bt2.place(x=450,y=180,width=200)

bt3 = ttk.Button(window,text="กด 2 ช็อกโกแลตลาวา",cursor="hand2",style="info.TButton",command=send_2)
bt3.place(x=750,y=180,width=230)

bt4 = ttk.Button(window,text="กด 3 บราวนี่ลาวา",cursor="hand2",style="info.TButton",command=send_3)
bt4.place(x=690,y=410,width=200)

bt5 = ttk.Button(window,text="ยืนยันการเลือกซื้อ",cursor="hand2",style="success.Solid.TButton",command=send_cf,)
bt5.place(x=1100,y=430,width=250)

bt6 = ttk.Button(window,text="ยกเลิกคำสั่งซื้อ",cursor="hand2",style="danger.Outline.TButton",command=send_cc)
bt6.place(x=1100,y=600,width=250)

window.mainloop()

