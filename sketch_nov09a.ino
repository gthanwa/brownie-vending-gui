#include <LedControl.h>
#include <LiquidCrystal_I2C.h>
LiquidCrystal_I2C lcd(0x27,16,2);
int DIN = 13;
int CS =  12;
int CLK = 11;
byte face[8]= {0x3C,0x42,0xA5,0x81,0xA5,0x99,0x42,0x3C};
LedControl lc=LedControl(DIN,CLK,CS,1);
int p1,p2,p3,p;
long int Ptotal;
char key;

void printByte(byte character [])
{
    int i = 0;
    for(i=0;i<8;i++)
    {
        lc.setRow(0,i,character[i]);
    }
}

void printprice()
{
        lcd.clear();
        lcd.setCursor(0,0);
        lcd.print("Price = ");
        lcd.print(Ptotal);
        delay(3000);
}

void Price()
{
  Ptotal = p1+p2+p3;
}


void setup()
{
    Serial.begin(9600);
    lc.shutdown(0,false);       
    lc.setIntensity(0,3);   
    lc.clearDisplay(0); 
    lcd.begin();
    lcd.setCursor(0,0);        
}

void loop()
{ 
   if(Serial.available())
        key = Serial.read();
   if (key == 's')   
      {
          printByte(face);
          lcd.clear();
          lcd.print("Welcome!");
          delay(2000); 
       }   
        
   if (key == '1')
       {
        p1 = p+39;    
        Price();
        printprice();       
       }
        
   if (key == '2')
       {
        p2 = p+35;
        Price();
        printprice();
       }

   if (key == '3')
       {
        p3 = p+25;
        Price();
        printprice();
       }

   if (key == 'f')
       {
        printprice();
        Serial.println(Ptotal);
       }

   if (key == 'c')
       {
        lcd.clear();
        lcd.setCursor(0,0);
        lcd.print("Thank You!");
        delay(6000);
       }
}
